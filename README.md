# AVKit Audio App

A SwiftUI demo project.

<table>
<thead>
   <tr>
        <th colspan="2"><div align="center">Screenshots</div></th>
  </tr>
</thead>
<tbody>
  <tr>
        <td>
         <img src="screenshots/screenshot1.png" width="200"> 
        </td>
        <td>
          <img src="screenshots/screenshot2.png" width="200">
        </td>
  </tr>
  <tr> 
     <th colspan="2">
     <div align="center">
        <b>Demo</b>
</div>
</th>
  </tr>
  <tr>
     <td colspan="3"> ![Demo Link](screenshots/demo.mp4) </td>
  </tr>
</tbody>
</table>
